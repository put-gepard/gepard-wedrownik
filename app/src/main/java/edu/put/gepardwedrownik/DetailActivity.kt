package edu.put.gepardwedrownik

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log

class DetailActivity : AppCompatActivity() {
    companion object {
        const val EXTRA_TRAILS_ID = "id"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail)

        val trailFrag: TrailDetailFragment? = supportFragmentManager.findFragmentById(R.id.detail_frag) as? TrailDetailFragment
        val trailsId = intent?.extras?.getInt(EXTRA_TRAILS_ID, 0)

        if (trailsId != null) {
            if (trailFrag != null) {
                trailFrag.setTrail(trailsId)
            } else {
                Log.d("Gepard", "Zielony")
            }
        }
    }
}