package edu.put.gepardwedrownik

class Trail(val name: String, val description: String) {
}

fun getTrails(): Array<Trail> {
    return arrayOf(
        Trail("Szlak górski", "Szlak górski w górach, ale nie nad wodą. Jest to trudny szlak bo nie ma wody."),
        Trail("Szlak morski", "Szlak morski w górach, ale nad wodą. Jest to trudny szlak bo jest woda."),
        Trail("Szlak lądowy", "Szlak lądowy na lądzie, ale nie w górach. Jest to prosty szlak bo jest ląd."),
        Trail("Szlak leśny", "Szlak leśny w lesie, ale nie w górach. Jest to średni szlak bo są drzewa."),
        Trail("Szlak pustynny", "Szlak pustynny na pustyni, ale nie nad wodą, Jest to trudny szlak bo nie ma wody do picia, ale za to jest piasek."),
    )
}
