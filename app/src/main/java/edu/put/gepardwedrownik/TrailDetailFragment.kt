package edu.put.gepardwedrownik

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.fragment.app.Fragment

class TrailDetailFragment : Fragment() {

    private var trailId: Int = 0

    companion object {
        private const val ARG_TRAIL_ID = "trailId"

        fun newInstance(trailId: Int): TrailDetailFragment {
            val fragment = TrailDetailFragment()
            val args = Bundle().apply {
                putInt(ARG_TRAIL_ID, trailId)
            }
            fragment.arguments = args
            return fragment
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_trail_detail, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        arguments?.let {
            trailId = it.getInt(ARG_TRAIL_ID)
        }
        setTrail(trailId)
    }

    fun setTrail(trailId: Int) {
        this.trailId = trailId
        val trail = getTrails()[trailId]
        val title: TextView? = view?.findViewById(R.id.textTitle)
        val description: TextView? = view?.findViewById(R.id.textDescription)
        title?.text = trail.name
        description?.text = trail.description
    }
}
