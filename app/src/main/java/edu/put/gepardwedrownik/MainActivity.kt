package edu.put.gepardwedrownik

import android.content.Intent
import android.os.Bundle
import android.widget.FrameLayout
import androidx.appcompat.app.AppCompatActivity

class MainActivity : AppCompatActivity(), TrailListFragment.Listener {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_main)

        val frameLayout= findViewById<FrameLayout>(R.id.fragment_container)
        if (frameLayout != null) {
            val newFragment = TrailDetailFragment.newInstance(0)
            supportFragmentManager.beginTransaction()
                .replace(R.id.fragment_container, newFragment)
                .addToBackStack(null)
                .commit()
        }
    }

    override fun itemClicked(id: Int) {
        val frameLayout= findViewById<FrameLayout>(R.id.fragment_container)
        if (frameLayout != null) {
            val newFragment = TrailDetailFragment.newInstance(id)
            supportFragmentManager.beginTransaction()
                .replace(R.id.fragment_container, newFragment)
                .addToBackStack(null)
                .commit()
        } else {
            val intent = Intent(this, DetailActivity::class.java)
            intent.putExtra(DetailActivity.EXTRA_TRAILS_ID, id)
            startActivity(intent)
        }

    }
}